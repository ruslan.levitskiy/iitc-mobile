// ==UserScript==
// @id             copier@voleon
// @name           IITC plugin: Portal link copier
// @category       Misc
// @version        1.1
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @description    Copy portal name and link to clipboard
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};


// PLUGIN START ////////////////////////////////////////////////////////

// use own namespace for plugin
    window.plugin.copier = function() {};
    window.plugin.copier.selectedGuid = null;
    window.plugin.copier.copy = function() {
        var input = $('#copyInput');
        input.show();
        var name = $('#portaldetails .title').text();
        var portal = window.portals[window.plugin.copier.selectedGuid];
        var latlng = portal.getLatLng();
        var lat = latlng.lat;
        var lng = latlng.lng;
        var url = 'https://www.ingress.com/intel?ll='+ lat + ',' + lng + '&z=17&pll=' + lat + ',' + lng;
        input.val(name + " " + url);
        copyfieldvalue('copyInput');
        input.hide();
    };

    function copyfieldvalue(id){
        var field = document.getElementById(id);
        field.focus();
        field.setSelectionRange(0, field.value.length);
        field.select();
        var copysuccess = copySelectionText();
    }

    function copySelectionText(){
        var copysuccess;
        try{
            copysuccess = document.execCommand("copy");
        } catch(e){
            copysuccess = false;
        }
        return copysuccess;
    }


    var setup =  function() {
        console.log('azaza');
        $('#toolbox').append('<a onclick="window.plugin.copier.copy();return false;" accesskey="c">Copy portal</a>');
        $('body').prepend('<input id="copyInput" style="position: absolute;"></input>');
        window.addHook('portalSelected', function(data){
            console.log(data);
            var guid = data.selectedPortalGuid;
            window.plugin.copier.selectedGuid = guid;
        });
    };

// PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


