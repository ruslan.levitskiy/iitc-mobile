// ==UserScript==
// @id             iitc-plugin-render-limit-increase@jonatkins
// @name           IITC plugin: render limit increase
// @category       Deleted
// @version        0.4.0.20181103.14414
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      none
// @downloadURL    none
// @description    [mobile-2018-11-03-014414] IITC no longer has simple render limits that can be adjusted - many more portals are now displayed without any increases required.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


